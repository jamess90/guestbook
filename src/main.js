// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import $ from "jquery";
import axios from "axios";
import "./css/commStyle.css";

Vue.prototype.$http = axios;
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: "#app",
  components: { App },
  template:
    '<App :innerWidth="innerWidth" :dataList="dataList" :form="form"></App>',
  data: {
    innerWidth: window.innerWidth,
    dataList: [],
    form: { name: "", password: "", bmk: "" }
  },
  created() {
    window.addEventListener("resize", this.handleResize);

    /* 댓글 최초로드 */
    this.getData();
    var rootThis = this;

    /* 10초간격으로 댓글창을 새로 로드한다. */
    var fn_interval = setInterval(function() {
      rootThis.getData();
    }, 10000);
  },
  destroyed() {
    window.removeEventListener("resize", this.handleResize);
    clearInterval(this.fn_interval);
  },
  methods: {
    handleResize: function() {
      this.innerWidth = window.innerWidth;
    },
    fnSubmit: function(name, password, bmk) {
      var rootThis = this;
      $.ajax({
        url:
          "https://script.google.com/macros/s/AKfycbzgxZ_WXOJWCeykhkRUzxaSf7r1uhB_9OpC4glfd5cVYrIwZL0/exec",
        data: { Name: name, Password: password, Bmk: bmk },
        type: "POST"
      }).then(function() {
        alert("등록되었습니다.");
        rootThis.form.name = "";
        rootThis.form.password = "";
        rootThis.form.bmk = "";
        rootThis.getData();
      });
    },

    // getGuestBookData() {
    //   var GSSurl =
    //     "https://spreadsheets.google.com/feeds/list/1AalDg4LGuXei0CVCktWi_ECFcCK6REITg_TNFiINWag/1/public/basic?alt=json-in-script&callback=?";
    //   $.getJSON(GSSurl, function(data) {
    //     this.dataList = data.feed.entry; //구글 스프레드 시트의 모든 내용은 feed.entry에 담겨있습니다.
    //   });
    // },

    // getGuestBookDataUsingAxios() {
    //   const baseURI =
    //     "https://spreadsheets.google.com/feeds/list/1AalDg4LGuXei0CVCktWi_ECFcCK6REITg_TNFiINWag/1/public/basic?alt=json-in-script&callback=?";
    //   this.$http.get(`${baseURI}/posts`).then(result => {
    //     console.log(result);
    //     this.dataList = result;
    //   });
    // },

    getData() {
      var _this = this;
      _this.loading = true;
      $.ajax({
        url:
          "https://spreadsheets.google.com/feeds/list/1AalDg4LGuXei0CVCktWi_ECFcCK6REITg_TNFiINWag/2/public/basic?alt=json-in-script&callback=?",
        type: "POST",
        dataType: "JSON",
        timeout: 30000
      })
        .done(function(response) {
          // 결과를 result에 저장, 각종 상태관리에 대한 변수를 완료상태로 변경
          _this.error = 0;
          _this.loading = false;
          _this.dataList = [];
          for (var item of response.feed.entry) {
            let temp = item.content.$t;
            temp = temp.split("이름: ");
            let tempArr = temp[1].split(", 내용: ");
            let tempArr2 = tempArr[1].split(", 시간: ");

            let name = tempArr[0];
            let bmk = tempArr2[0];
            let dataTime = tempArr2[1];

            _this.dataList.push({
              name: name,
              bmk: bmk,
              time: dataTime,
              bmkBlockBgColor: {
                backgroundColor: "white"
              }
            });
          }
        })
        .fail(function(error) {
          // 통신오류시 다시 시도
          // 재시도 횟수가 지정된 수에 도달하면 상태관리에 대한 변수를 업데이트 ajax중지
          if (_this.error <= 5) {
            _this.error++;
            _this.getData();
          } else {
            _this.error = true;
            _this.loading = false;
          }
        });
    }
  }
});
